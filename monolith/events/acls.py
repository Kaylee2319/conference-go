import json
import requests
from .keys import PEXELS_API_KEY, OPEN_WEATHER_API_KEY


def get_photo(city, state):
    headers = {"Authorization": PEXELS_API_KEY}
    url = f"https://api.pexels.com/v1/search?query={city},{state}"
    response = requests.get(url, headers=headers)
    data = json.loads(response.text)
    picture_url = data["photos"][0]["src"]["medium"]

    return picture_url


def get_weather_data(city, state):
    geo_url = f"https://api.openweathermap.org/geo/1.0/direct?q={city},{state},USA&appid={OPEN_WEATHER_API_KEY}"
    geo_response = requests.get(geo_url)
    geo_data = json.loads(geo_response.text)
    lat = geo_data[0]["lat"]
    lon = geo_data[0]["lon"]
    weather_url = f"https://api.openweathermap.org/data/2.5/weather?lat={lat}&lon={lon}&units=imperial&appid={OPEN_WEATHER_API_KEY}"
    weather_response = requests.get(weather_url)
    weather_data = json.loads(weather_response.text)
    temp = weather_data["main"]["temp"]
    description = weather_data["weather"][0]["description"]

    return {"temp": temp, "description": description}
